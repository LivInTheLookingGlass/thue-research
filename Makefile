size ?= letter

test_remote:
	cd .. && python3 -m pytest -lv -d thue --tx=8*popen//python3 \
	--tx=8*ssh=gappleto97@10.10.0.1//python=python3 \
	--tx=3*ssh=gappleto@williaccluster.nmu.edu//python=python3 \
	--tx=8*ssh=gappleto@euclid.nmu.edu//python=python3 \
	--rsyncignore=*.pdf* --rsyncignore=*.rst  --rsyncignore=log.txt \
	--rsyncdir=thue --rsyncignore=__pycache__ --rsyncignore=Makefile \
	| tee log.txt

test_remote_williac:
	cd .. && python3 -m pytest -lv -d thue \
	--tx=6*ssh=gappleto@williaccluster.nmu.edu//python="mpirun -nolocal python3" \
	--rsyncdir=thue --rsyncignore=__pycache__ --rsyncignore=Makefile \
	--rsyncignore=*.pdf* --rsyncignore=*.rst

test:
	python3 -m pytest -lv .

test_2:
	python3 -m pytest -lv -d -n2 .

test_3:
	python3 -m pytest -lv -d -n3 .

pdf:
	@echo "Generating a(n) $(size) pdf"
	@sed 1,6d README.rst > .thue.rst
	@./pandoc -f rst+implicit_header_references -o thue.$(size).pdf .thue.rst \
	--include-in-header preamble.tex --pdf-engine=xelatex \
	-V mainfont="DejaVu Sans" -V monofont="Liberation Mono" \
	-V sansfont="DejaVu Sans" -V geometry:margin=15mm -V papersize=$(size) || \
	echo "download a pandoc nightly"
	@rm .thue.rst

letter:
	@$(MAKE) pdf size=letter

legal:
	@$(MAKE) pdf size=legal

A4:
	@$(MAKE) pdf size=A4

all: letter A4 legal
