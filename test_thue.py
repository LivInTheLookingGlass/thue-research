from math import sqrt, ceil, log
from itertools import islice

from .thue import (thue_gen, thue_n, thue_base, thue, thue_nr_gen, thue_nr,
                   thue_rotation)
from pytest import fixture

import os

os.system("renice -n 20 {}".format(os.getpid()))


def partition(sequence, count):
    """Yields count character chunks of the string sequence"""
    return map(tuple, zip(*[iter(sequence)] * count))


def sequence(n, count):
    yield from (x for x, _ in zip(thue_rotation(n), range(count)))


@fixture(params=range((2**15) + 1, int(2**15.5) + 1))
def n(request):
    """A fixture that py.test will use to generate more tests wherever n is an
    argument to the test"""
    return request.param


def cycle_metric(sequence, players):
    """If moves are valued based on who moves first in a round, rather than by
    their absolute position in the game, this is the metric to use"""
    total = [0] * players
    for part in partition(sequence, players):
        for index, player in enumerate(part):
            total[player] += players - index
    return total


def absolute_metric(sequence, players):
    """If moves are valued based on their absolute position in the game, rather
    than by who moves first in a round, this is the metric to use. Note that
    it is done in "golf scores", so a lower number is better. This is because
    it's slightly more efficient to implement that way"""
    total = [0] * players
    for idx, player in enumerate(sequence):
        total[player] += idx
    return total


def test_thue_2():
    """Checks that thue() gives an identical sequence to thue_n(2)"""
    for idx, right, check in zip(range(2**32), thue_gen(), thue_n(2)):
        assert right == check, "At {}, {} != {}".format(idx, check, right)


def test_thue_recursive():
    """Checks that the new recursive definition mapping works"""
    for x, right, check in zip(range(2**24), thue_gen(), thue_nr_gen(2)):
        assert right == check, "At {}, {} != {}".format(x, check, right)


def test_thue_base(n):
    actual_test_thue_base(n)


def actual_test_thue_base(n):
    """Checks that counting in base is equivalent to substitutions"""
    for idx, x, check in zip(range(2**24), thue_base(n), thue_rotation(n)):
        assert x == check, "At {}, {} != {}".format(idx, check, x)


def test_thue_rotation(n):
    actual_test_thue_rotation(n)


def actual_test_thue_rotation(n):
    """Checks that standing rotations is equivalent to substitutions"""
    for idx, x, check in zip(range(2**24), thue_rotation(n), thue_nr_gen(n)):
        assert x == check, "At {}, {} != {}".format(idx, check, x)


def test_thue_substitution(n):
    actual_test_thue_substitution(n)


def actual_test_thue_substitution(n):
    """Checks that the new recursive definition mapping works"""
    for idx, right, check in zip(range(2**24), thue_n(n), thue_rotation(n)):
        assert right == check, "At {}, {} != {}".format(idx, check, right)


def test_thue_n_regularity(n):
    actual_test_thue_n_regularity(n)


def actual_test_thue_n_regularity(n):
    count = ceil(log(2**28, n))
    partitions = tuple([0] * count for _ in range(n))
    seq = enumerate(sequence(n, n**count))
    resume = 0
    for x in range(1, count):
        for index, letter in islice(seq, n**x - resume - 1):
            resume = index
            for i in range(count):
                partitions[letter][i] += index**i
        compare, *rest = partitions
        for r in rest:
            for power in range(x - 1):
                assert compare[power] == r[power]


def test_thue_n_cycle_fairness(n):
    actual_test_thue_n_cycle_fairness(n)


def actual_test_thue_n_cycle_fairness(n):
    """Ensures that thue_n(n) yields a cyclically fair sequence"""
    count = n**ceil(log(2**20, n))
    a, *rest = cycle_metric(sequence(n, count), n)
    for x in rest:
        assert a == x


# def test_thue_n_absolute_fairness(n):
#     actual_test_thue_n_absolute_fairness(n)


# def actual_test_thue_n_absolute_fairness(n):
#     """Ensures that thue_n(n) yields an absolutely fair sequence"""
#     count = n**int(sqrt(40 - n))
#     a, *rest = absolute_metric(sequence(n, count), n)
#     for x in rest:
#         assert a == x
