.. contents:: Table of Contents
    :backlinks: none
    :depth: 2

.. sectnum::

:authors: Gabe Appleton, Daniel Rowe (PhD)

#################################
Extending the Thue-Morse Sequence
#################################

========
Abstract
========

Extend the Thue-Morse sequence from binary to n-ary in a way that keeps as many
useful properties as possible, and demonstrate the extended definitions
equivalent, both to each other and the original sequence. The properties
intentionally kept are:

* Prouhet-Tarry-Escott generalization [BORZ]_
* Fair turn orders
* recurrence without periodicity (needs to be looked into)

============
Introduction
============

The Thue-Morse sequence has turned up several times over the years. It has been
independently discovered several times, including by chess grandmaster Max Euwe
[EUWE]_, who used it to demonstrate how to avoid the Threefold Repetition rule
in chess, leading to games of potentially infinite length.

It has also been shown to be a fair turn ordering in games where later moves are
less valuable than earlier ones. In 2001, Robert Richman showed this for games
where this decrease in value is continuous [RMAN]_, and in 2013, Cooper and
Dutle showed this for discrete decreases [JCAD]_.

It has also been shown to be a solution to the Prouhet-Tarry-Escott problem,
which asks for a partitioning scheme of a set such that the sums of many powers
are equal between the two partitions. For example, with the set
:math:`\{1, 2, ..., 7, 8\}`:

.. math::

    \begin{aligned}
    1^0 + 4^0 + 6^0 + 7^0 &= 2^0 + 3^0 + 5^0 + 8^0 &\\
                        4 &= 4 \texttt{, and} &\\
    1^1 + 4^1 + 6^1 + 7^1 &= 2^1 + 3^1 + 5^1 + 8^1 &\\
                       18 &= 18 \texttt{, and} &\\
    1^2 + 4^2 + 6^2 + 7^2 &= 2^2 + 3^2 + 5^2 + 8^2 &\\
         1 + 16 + 36 + 49 &= 4 + 9 + 25 + 64 &\\
                      102 &= 102
    \end{aligned}

=====
Notes
=====

All functions are assumed to operate on :math:`ℕ` (including 0).

In addition, we are borrowing a shorthand from the Python programming language.
When you see: :math:`\comprehension{function(x)}{x}{sequence}`, what
this means is "construct a tuple such that each entry is the result of
:math:`function(x)`, where :math:`x` is the corresponding entry in
:math:`sequence`".

=======================
The Thue-Morse Sequence
=======================

The original Thue-Morse sequence begins as follows:

.. math::

    \begin{aligned}
    T_2 &= \tuple{T_2(x) \; for \; x \; in \; ℕ} &\\
        &= \tuple{0, 1, 1, 0, 1, 0, 0, 1, 1, 0, 0, 1, 0, 1, 1, 0, 1, 0, 0, 1,
    0, 1, 1, 0, 0, 1, 1, 0, 1, 0, 0, 1, 1, 0, 0, 1, 0, 1, 1, 0, 0, 1, 1, 0, 1,
    0, 0, 1, 0, 1, 1, 0, ...}
    \end{aligned}

It has at least six equivalent definitions. Each will be listed below, as well
as our proposed extensions of them into sequences of n-ary digits. For each of
these definitions :math:`x` will represent the position in the sequence that
you are trying to find, and :math:`n` will represent the alphabet size. So in
the above sequence it would be :math:`n=2`.

=====================
Binary Definition One
=====================

The entry :math:`T_2(x)` can be calculated by taking the index :math:`x` in
binary, and considering the sum of its binary digits modulo :math:`2`. Or in a
compact mathematical notation:

.. math::

    T_2(x) = \sum_{k=0}^{\ceil*{\log_2(x)}} \floor*{\dfrac{x}{2^{k}}} \pmod{2}

Where :math:`\floor*{.}` and :math:`\ceil*{.}` represent the floor and ceiling
functions.

This definition is popular among programmers in low level languages because it
can be computed very efficiently. It takes :math:`Θ(1)` time to generate each
position in the sequence, and only :math:`Θ(\log_2(x))` memory. If you go far
enough in the sequence that you can no longer index by a primitive number type,
then it takes :math:`Θ(\log_2(x))` operations per item.

=======================
Extended Definition One
=======================

The derivation here is easy, just replace all instances of :math:`2` with
:math:`n`.

.. math::

    T_n(x) = \sum_{k=0}^{\ceil*{\log_n(x)}} \floor*{\dfrac{x}{n^{k}}} \pmod{n}

Therefore, the :math:`x`-th entry of :math:`T_n` is given by the esum of the
base :math:`n` digits of :math:`x`, modulo :math:`n`.

=====================
Binary Definition Two
=====================

This definition is given in terms of a recursive function, and is also
frequently used by programmers and computer scientists.

.. math::

    \begin{aligned}
       T_2(0) &= 0 &\\
      T_2(2x) &= T_2(x) &\\
    T_2(2x+1) &= 1 - T_2(x)
    \end{aligned}

Noting that the outputs are exclusively 0 or 1, this can be simplified as
follows:

.. math::

    \begin{aligned}
                  T_2(0) &= 0 &\\
                 T_2(2x) &= T_2(x) &\\
                         &= 2x + T_2(x) \pmod{2} &\\
               T_2(2x+1) &= 1 - T_2(x) &\\
                         &= 1 + T_2(x) \pmod{2} &\\
                         &= (2x + 1) + T_2(x) \pmod{2} &\\
                         &= 2x + 1 + T_2(x) \pmod{2} &\\
    \therefore \; T_2(x) &= x + T_2(\floor*{\tfrac{x}{2}}) \pmod{2}
    \end{aligned}

So written at its simplest, it looks like:

.. math::

    \begin{aligned}
    T_2(0) &= 0 &\\
    T_2(x) &= x + T_2(\floor*{\tfrac{x}{2}}) \pmod{2}
    \end{aligned}

=======================
Extended Definition Two
=======================

The derivation here is also fairly intuitive. Just replace all instances of
:math:`2` with :math:`n`.

.. math::

    \begin{aligned}
    T_n(0) &= 0 &\\
    T_n(x) &= x + T_n(\floor*{\tfrac{x}{n}}) \pmod{n}
    \end{aligned}

=======================
Binary Definition Three
=======================

This definition is a popular interpretation. It imagines the sequence as a turn
order where each player "takes turns, taking turns, taking turns...".

To picture this, imagine two captains, A and B, choosing a pickup basketball
team. For an initial choosing order, they alternate: AB. In order to "take turns
taking turns" they alternate who goes first, yielding ABBA.

But they are supposed to "take turns taking turns" at each possible level, so
they again take the original sequence and alternate who goes first, yielding
ABBABAAB.

In a compact mathematical notation:

.. math::

    T_2(x) = rotate\left(\tuple{0, 1}, \sum_{k=0}^{\ceil*{\log_2(x)}} \floor*{\dfrac{x}{2^{k}}}\right)_0

Where

.. math::

    \begin{aligned}
             \tuple{a, b, c}_0 &= a &\\
    rotate(\tuple{a, b, c}, 0) &= \tuple{a, b, c} &\\
    rotate(\tuple{a, b, c}, 1) &= \tuple{b, c, a} &\\
    rotate(\tuple{a, b, c}, 2) &= \tuple{c, a, b} \texttt{, etc.}
    \end{aligned}

So the process of building up this sequence looks something like:

.. math::

    \begin{aligned}
    T_2(0) &= rotate(\tuple{0, 1}, \floor*{\tfrac{0}{1}})_0 = rotate(\tuple{0, 1}, 0)_0 = 0 &\\
    T_2(1) &= rotate(\tuple{0, 1}, \floor*{\tfrac{1}{1}} + \floor*{\tfrac{1}{2}})_0 = rotate(\tuple{0, 1}, 1)_0 = 1 &\\
    T_2(2) &= rotate(\tuple{0, 1}, \floor*{\tfrac{2}{1}} + \floor*{\tfrac{2}{2}} + \floor*{\tfrac{2}{4}})_0 = rotate(\tuple{0, 1}, 3)_0 = 1 &\\
    T_2(3) &= rotate(\tuple{0, 1}, \floor*{\tfrac{3}{1}} + \floor*{\tfrac{3}{2}} + \floor*{\tfrac{3}{4}})_0 = rotate(\tuple{0, 1}, 4)_0 = 0 &\\
    \vdots &= \; \vdots
    \end{aligned}

=========================
Extended Definition Three
=========================

The derivation here is easy, just replace all instances of :math:`2` with
:math:`n`.

.. math::

    T_n(x) = rotate\left(\range{n}, \sum_{k=0}^{\ceil*{\log_n(x)}} \floor*{\dfrac{x}{n^{k}}}\right)_0

======================
Binary Definition Four
======================

This is a substitution definition. Starting at :math:`\tuple{0}`, substitute
each entry, and flatten down to a tuple of integers.

.. math::

    0 \rightarrow \tuple{0, 1}

    1 \rightarrow \tuple{1, 0}

From it we can derive some interesting constraints.

1. When making a substitution, the part already constructed remains static.
2. Both substitutions use every digit available in that base.
3. The substitution keeps the first turns in numerical order.
4. Each substitution rule is the same length.

========================
Extended Definition Four
========================

Starting at :math:`\tuple{0}`, substitute each entry, and flatten down to a
tuple of integers.

.. math::

    0 \rightarrow \tuple{0, 1, 2, \hdots, n-1}

    1 \rightarrow \tuple{1, 2, \hdots, n-1, 0}

    2 \rightarrow \tuple{2, \hdots, n-1, 0, 1}

    \vdots

    n-1 \rightarrow \tuple{n-1, 0, 1, 2, \hdots}

Note that each substitution is the ordered set of players rotated by the number
being substituted.

Also noteworthy is that this can be interpreted as a normalized Latin
Square, as described in [BORZ]_. We expand on this further in
`Prouhet-Terry-Escott Generalization`_.

Their substitution rules produce an identical series, assuming you start at
:math:`\tuple{0}`. This means that Prouhet-Terry-Escott generalization is
preserved under this definition. We have verified this property for alphabet
sizes :math:`n \in [2, 2^{15}]` for sequences of up to length :math:`n^x`,
where :math:`n^x \geq 2^{28} > n^{x-1}`.

An approach that *does not* work is something like integer sequence [A007413]_:

.. math::

    0 \rightarrow \tuple{0, 1, 2}

    1 \rightarrow \tuple{0, 2}

    2 \rightarrow \tuple{1}

This is not desirable for several reasons

1. It does not follow from the original sequence
2. It does not allow for Prouhet-Terry-Escott generalization

======================
Binary Definition Five
======================

Starting with 0, take the current sequence and do a bitwise inversion on it,
then concatenate the result to the current sequence.

.. math::

    \begin{aligned}
    t_2(0) &= \tuple{0} &\\
    t_2(k) &= t_2(k-1) \| invert(t_2(k-1)) &\\
    T_2(x) &= t_2(\ceil*{\log_2(x)})_x
    \end{aligned}

For our purposes, a bitwise inversion can be defined as:

.. math::

    \begin{aligned}
           invert(0) &= 1 &\\
           invert(1) &= 0 &\\
    invert(sequence) &= \tuple{invert(x) \; for \; x \; in \; sequence}
    \end{aligned}

Unfortunately, this definition has two extensions, depending on how you
generalize a bitwise inversion.

========================
Extended Definition Five
========================

This extension takes the view that what's actually happening is a set of nested
rotations, halting when you get to a single bit. The logic here goes that, since
each of these numbers are going to come in groups of :math:`n`, you can just
rotate that set of :math:`n` numbers. A bit like:

.. math::

    \begin{aligned}
                            0 &= rotate(\tuple{0, 1}, 0)_0 &\\
                            1 &= rotate(\tuple{0, 1}, 0)_1 &\\
              \therefore \; a &= rotate(\tuple{0, 1}, 0)_a \pmod{2} &\\
    \texttt{Also:} &\\
                    invert(0) &= rotate(\tuple{0, 1}, 1)_0 &\\
                    invert(1) &= rotate(\tuple{0, 1}, 1)_1 &\\
      \therefore \; invert(a) &= rotate(\tuple{0, 1}, 1)_a \pmod{2} &\\
    \therefore \; invert^b(a) &= rotate(\tuple{0, 1}, b)_a \pmod{2}
    \end{aligned}

You can pretty easily extend that logic to make a function that looks like:

.. math::

    \begin{aligned}
    t_n(0) &= \range{n} &\\
    t_n(k) &= \tuple{rotate(t_n(k-1), L) \; for \; L \; in \; \range{n}} &\\
    T_n(x) &= flatten(t_n(\ceil*{\log_n(x)}))_x
    \end{aligned}

=======================
Extended Definition Six
=======================

This extension takes the view that what's really happening is an increment on
each sequence item, staying in mod :math:`n`. The logic here is that:

.. math::

    \begin{aligned}
               \tuple{0, 1, 1, 0} &= \tuple{0+0, 1+0, 1+0, 0+0} \pmod{2} &\\
       invert(\tuple{0, 1, 1, 0}) &= \tuple{0+1, 1+1, 1+1, 0+1} \pmod{2} &\\
     invert(invert(\tuple{0, 1, 1, 0})) &= \tuple{0+2, 1+2, 1+2, 0+2} \pmod{2} &\\
    \therefore \; invert^b(tuple) &= \tuple{a + b \; for \; a \; in \; tuple} \pmod{2} &\\
                                  &= increment(tuple, b) \pmod{2}
    \end{aligned}

Given the above, it's fairly easy to generalize `Binary Definition Five`_.

.. math::

    \begin{aligned}
    t_n(0) &= \range{n} \pmod{n} &\\
    t_n(k) &= \concat{n-1}{r=0} increment(t_n(k-1), r) \pmod{n} &\\
    T_n(x) &= t_n(\ceil*{\log_n(x)})_x
    \end{aligned}

=====================
Binary Definition Six
=====================

This definition looks at a generating sequence. Once expanded, it produces an
infinite series where the coefficients of each term represents the value of
:math:`T_2` at that term's power.

.. math::

    \begin{aligned}
    \prod_{i=0}^{\infty} (1 - x^{2^{i}}) &= (1-x) \cdot (1-x^2) \cdot (1-x^4) \cdot \hdots &\\
                                         &= 1 - x - x^2 + x^3 - x^4 + x^5 + x^6 - x^7 + \hdots &\\
                                         &= \sum_{j=0}^{\infty} (-1)^{T_2(j)} x^{j}
    \end{aligned}

=========================
Extended Definition Seven
=========================

The derivation here is somewhat more complicated than the rest. An initial guess
was just to interpret :math:`-1` as a root of unity. Unfortunately, that does
not work except in the case of :math:`n=2`. In order to generate the correct
sequence, there needs to be additional consideration for *why* the generating
sequence works. It works because you can uniquely represent any counting number
as a sum of positive powers of 2, like you see in binary. But to make this work
in n-ary you need to allow for other digits. The generating sequence has to look
like:

.. math::

    \texttt{Where } \zeta = \zeta_n

    \prod_{i=0}^{\infty} \left[\sum_{k=0}^{n-1} (\zeta^k \cdot x^{k \cdot n^i})\right]
    = \sum_{j=0}^{\infty} (\zeta^{T_n(j)} \cdot x^j)

To verify that the binary version still works, we can plug in :math:`n=2`:

.. math::

    \begin{aligned}
    \prod_{i=0}^{\infty} \left[\sum_{k=0}^{n-1} (\zeta^k x^{k \cdot n^i})\right]
    &= \prod_{i=0}^{\infty} \left[\sum_{k=0}^{1} (\zeta^k x^{k \cdot 2^i})\right] &\\
    &= \prod_{i=0}^{\infty} (1 - x^{2^i}) &\\
    &= (1-x) \cdot (1-x^2) \cdot (1-x^4) \cdot \hdots &\\
    &= 1 - x - x^2 + x^3 - x^4 + x^5 + x^6 - x^7 + \hdots &\\
    &= \sum_{j=0}^{\infty} (-1)^{T_2(j)} \cdot x^{j} &\\
    &= \sum_{j=0}^{\infty} (\zeta^{T_2(j)} \cdot x^j)
    \end{aligned}

And to show it for :math:`n=3`:

.. math::

    \begin{aligned}
    \prod_{i=0}^{\infty} \left[\sum_{k=0}^{n-1} (\zeta^k x^{k \cdot n^i})\right]
    &= \prod_{i=0}^{\infty} \left[\sum_{k=0}^{1} (\zeta^k x^{k \cdot 3^i})\right] &\\
    &= \prod_{i=0}^{\infty} (1 + \zeta x^{3^i} + \zeta^2 x^{2 \cdot 3^i}) &\\
    &= (1 + \zeta x + \zeta^2 x^2) \cdot (1 + \zeta x^3 + \zeta^2 x^6) \cdot \hdots &\\
    &= 1 + \zeta x + \zeta^2 x^2 + \zeta x^3 + \zeta^2 x^4 + x^5 + \zeta^2 x^6 + x^7 + \zeta x^8 + \hdots &\\
    &= \sum_{j=0}^{\infty} (\zeta^{T_3(j)} \cdot x^j)
    \end{aligned}

===================
Efficient Generator
===================

This family of sequences can be efficiently generated using the following
Python code. It takes :math:`Θ(n \cdot \log_a(n))` operations and
:math:`O(\log_a(n)), Ω(a)` memory, where n is the number of sequence items
generated, and a is the alphabet size. Converting this to return the sequence as
a list would make it require :math:`O(n \cdot \log_a(n)), Ω(n)` memory.

This is sadly less efficient than could be done for the binary version, where
one can generate the sequence using :math:`Θ(n)` operations, but is equally
memory efficient.

The method used here is shown in `Extended Definition Three`_.

.. code-block:: python

    def sum_of_digits(x, base):
        if x < base:
            return x
        return x + sum_of_digits(x // base, base)


    def thue_rotation(a):
        alphabet = tuple(x for x in range(a))
        iteration = 0
        while True:                            # n ops
            idx = sum_of_digits(iteration, a)  # n*log_a(n) ops, log_a(n) memory
            for i in range(a):
                yield alphabet[(idx + i) % a]  # n*a ops
            iteration += 1                     # n ops

=================================
Showing Equivalence of Extensions
=================================

+++++++++++++++++++++++++++++++++++++++++++++
Showing Equivalence of Extensions One And Two
+++++++++++++++++++++++++++++++++++++++++++++

`Extended Definition One`_ and `Extended Definition Two`_ turn out to be
algebraically equivalent.

.. math::

    \begin{aligned}
    T_n(x) &= x + T_n(\floor*{\tfrac{x}{n}}) \pmod{n} &\\
           &= x + \floor*{\tfrac{x}{n}} + T_n(\floor*{\tfrac{x}{n^2}}) \pmod{n} &\\
           &= x + \floor*{\tfrac{x}{n}} + \floor*{\tfrac{x}{n^2}}
              + T_n(\floor*{\tfrac{x}{n^3}}) \pmod{n} &\\
           &= x + \floor*{\tfrac{x}{n}} + \floor*{\tfrac{x}{n^2}}
              + \floor*{\tfrac{x}{n^3}} + \hdots \pmod{n} &\\
           &= \sum^{\ceil*{\log_n(x)}}_{k=0} \floor*{\dfrac{x}{n^k}} \pmod{n}
    \end{aligned}

This relies on the following claim:

.. math::

    \texttt{If } a, b, r, x, y \in ℤ \texttt{ and } y > 0, \texttt{ then}

    \floor*{\dfrac{\floor*{\tfrac{x}{y}}}{y}} = \floor*{\dfrac{x}{y^2}}

Proof:

.. math::

    x = ay + r \texttt{, where r < y}

    \tfrac{x}{y} = a + \tfrac{r}{y}

    \floor*{\tfrac{x}{y}} = a

    \floor*{\dfrac{\floor*{\tfrac{x}{y}}}{y}} = \floor*{\dfrac{a}{y}}

Now that this is in a simpler form, we will manipulate the right hand side to
produce an inequality, where :math:`\floor*{\tfrac{a}{y}}` is the minimum value.

.. math::

    \tfrac{x}{y} = a + \tfrac{r}{y}

    \floor*{\tfrac{x}{y^2}} = \floor*{\tfrac{a}{y} + \tfrac{r}{y^2}}

Since :math:`\tfrac{r}{y^2} \in [0, \tfrac{y-1}{y^2}]`, set these up as upper and
lower bounds, substituting for :math:`\tfrac{r}{y^2}`.

.. math::

    \floor*{\tfrac{a}{y}} \leq \floor*{\tfrac{x}{y^2}} \leq \floor*{\tfrac{ay+y-1}{y^2}}

    \floor*{\tfrac{a}{y}} \leq \floor*{\tfrac{x}{y^2}} \leq \floor*{\tfrac{a+1}{y} - \tfrac{1}{y^2}}

To demonstrate that this works on the integers, we will consider the edge cases,
where b is an arbitrary integer, of :math:`a = by - 1`

.. math::

    \begin{aligned}
    \floor*{\tfrac{a}{y}} &= \floor*{\tfrac{by-1}{y}} \leq \floor*{\tfrac{x}{y^2}} \leq \floor*{\tfrac{by-1+1}{y} - \tfrac{1}{y^2}} &\\
    \floor*{\tfrac{a}{y}} &= \floor*{b - \tfrac{1}{y}} \leq \floor*{\tfrac{x}{y^2}} \leq \floor*{b - \tfrac{1}{y^2}} &\\
    \floor*{\tfrac{a}{y}} &= b - 1 \leq \floor*{\tfrac{x}{y^2}} \leq b - 1 &\\
    \floor*{\tfrac{a}{y}} &= \floor*{\tfrac{x}{y^2}}
    \end{aligned}

And :math:`a = by`

.. math::

    \begin{aligned}
    \floor*{\tfrac{a}{y}} &= \floor*{\tfrac{by}{y}} \leq \floor*{\tfrac{x}{y^2}} \leq \floor*{\tfrac{by+1}{y} - \tfrac{1}{y^2}} &\\
    \floor*{\tfrac{a}{y}} &= \floor*{b} \leq \floor*{\tfrac{x}{y^2}} \leq \floor*{b + \tfrac{1}{y} - \tfrac{1}{y^2}} &\\
    \floor*{\tfrac{a}{y}} &= b \leq \floor*{\tfrac{x}{y^2}} \leq \floor*{b + \tfrac{y - 1}{y^2}} &\\
    \floor*{\tfrac{a}{y}} &= b \leq \floor*{\tfrac{x}{y^2}} \leq b &\\
    \floor*{\tfrac{a}{y}} &= \floor*{\tfrac{x}{y^2}}
    \end{aligned}

This proves our claim.

In addition, we have tested their equivalence of these definitions (via
definition 3) in sequences in the range
:math:`x \in [0, 2^{24}], n \in [2, 2^{15}]`.

+++++++++++++++++++++++++++++++++++++++++++++++
Showing Equivalence of Extensions One And Three
+++++++++++++++++++++++++++++++++++++++++++++++

In order to show these definitions equivalent, we must first demonstrate two
properties.

First, how rotation relates to modular arithmetic, under the class of tuples
we use. All tuples we use will be of the form:

.. math::

    \tuple{x, x+1, x+2, ..., x+n-1} \pmod{n}

    \therefore

    \tuple{x, x+1, x+2, ..., x+n-1}_y = x + y \pmod{n}

    \therefore

    rotate(\tuple{x, x+1, x+2, ..., x+n-1}, y)_0 = \tuple{x+y, x+y+1, x+y+2, ..., x+y+n-1}_0 =  x + y \pmod{n}

    \therefore

    rotate(\tuple{x, x+1, x+2, ..., x+n-1}, y)_z = \tuple{x+y, x+y+1, x+y+2, ..., x+y+n-1}_z = x + y + z \pmod{n}

Using this relation, we can rewrite `Extended Definition Three`_ like so:

.. math::

    \texttt{Where } y = \sum_{k=0}^{\ceil*{\log_n(x)}} \floor*{\dfrac{x}{n^{k}}}

    rotate(\tuple{0, 1, 2, \hdots, n-1}, y)_0 = \tuple{y, y+1, y+2, \hdots, y+n-1}_0
        = y = \sum_{k=0}^{\ceil*{\log_n(x)}} \floor*{\dfrac{x}{n^{k}}} \pmod{n}

This is identical to `Extended Definition One`_. In addition, we have tested
the equivalence of these definitions in the range
:math:`x \in [0, 2^{24}], n \in [2, 2^{15}]`.

+++++++++++++++++++++++++++++++++++++++++++++
Showing Equivalence of Extensions One And Six
+++++++++++++++++++++++++++++++++++++++++++++

`Extended Definition One`_ and `Extended Definition Six`_ turn out to be
rephrasings of each other. For :math:`k=0`, it is a one digit number (base n),
and is identical by definition. For :math:`k>0` each iteration emulates counting
in base n. For :math:`n=3`:

.. math::

    \begin{aligned}
    k&=0: \tuple{0,1,2} \pmod{3} &\\
    k&=1: \tuple{0+0,0+1,0+2} \| \tuple{1+0,1+1,1+2} \| \tuple{2+0,2+1,2+2} \pmod{3}
    \end{aligned}

+++++++++++++++++++++++++++++++++++++++++++++++
Showing Equivalence of Extensions One And Seven
+++++++++++++++++++++++++++++++++++++++++++++++

`Extended Definition One`_ and `Extended Definition Seven`_ can be shown to be
equivalent by analyzing why the :math:`\zeta` in `Extended Definition Seven`_
has the powers that it does, for a given position. To do this, we'll bring back
the generating sequence:

.. math::

    \begin{aligned}
    \prod_{i=0}^{\infty} \left[\sum_{k=0}^{n-1} (\zeta^k x^{k \cdot n^i})\right]
    &= \prod_{i=0}^{\infty} \left[\sum_{k=0}^{2} (\zeta^k x^{k \cdot 3^i})\right] &\\
    &= \prod_{i=0}^{\infty} (\zeta^0 x^{0 \cdot 3^i} + \zeta^1 x^{1 \cdot 3^i} + \zeta^2 x^{2 \cdot 3^i})
    \end{aligned}

This sequence encodes the ways that you can uniquely represent a number in base
:math:`n`. Take for example 17 in base 3.

.. math::

    17 = 1 \cdot 3^2 + 2 \cdot 3^1 + 2 \cdot 3^0 = 122_3

    \begin{aligned}
    \prod_{i=0}^{\infty} \left[\sum_{k=0}^{2} (\zeta^k x^{k \cdot 3^i})\right]
    &= \prod_{i=0}^{\infty} (\zeta^0 x^{0 \cdot 3^i} + \zeta^1 x^{1 \cdot 3^i} + \zeta^2 x^{2 \cdot 3^i}) &\\
    &= (\zeta^0 x^{0 \cdot 3^0} + \zeta^1 x^{1 \cdot 3^0} + \boxed{\zeta^2 x^{2 \cdot 3^0}}) \cdot
       (\zeta^0 x^{0 \cdot 3^1} + \zeta^1 x^{1 \cdot 3^1} + \boxed{\zeta^2 x^{2 \cdot 3^1}}) \cdot
       (\zeta^0 x^{0 \cdot 3^2} + \boxed{\zeta^1 x^{1 \cdot 3^2}} + \zeta^2 x^{2 \cdot 3^2}) \cdot ...
    \end{aligned}

To find the coefficient at :math:`x^{17}` we need to select the appropriate
products from each grouping. They have been boxed above. From them, we can see:

.. math::

    \begin{aligned}
    \zeta^{T_3(17)} \cdot x^{17}
    &= (\zeta^2 x^{2 \cdot 3^0}) \cdot (\zeta^2 x^{2 \cdot 3^1}) \cdot (\zeta^1 x^{1 \cdot 3^2}) \cdot 1 \cdot 1 \cdot ... &\\
    &= (\zeta^2 x^2) \cdot (\zeta^2 x^6) \cdot (\zeta x^9) \cdot 1 \cdot 1 \cdot ... &\\
                          &= \zeta^{2+2+1} \cdot x^{2+6+9} &\\
                          &= \zeta^5 \cdot x^{17} &\\
                          &= \zeta^2 \cdot x^{17} &\\
          \zeta^{T_3(17)} &= \zeta^2 &\\
    \therefore    T_3(17) &= 2, \texttt{ which is the sum (modulo 3) of the digits of 17 in base 3}
    \end{aligned}

Because we know that you can uniquely represent any number in base
:math:`n`, the coefficient of :math:`x^j` arises in only one possible way. As
the above example illustrates, this coefficient will equal
:math:`\zeta^{T_n(j)}`.

++++++++++++++++++++++++++++++++++++++++++++++++
Showing Equivalence of Extensions Three And Four
++++++++++++++++++++++++++++++++++++++++++++++++

It is helpful to think of `Extended Definition Four`_ in base n. This allows you
to see the rotations that show up in the produced sequence. At three iterations,
the substitution definition produces:

.. math::

    \fbox{\fbox{\fbox{0}\fbox{1}\fbox{2}} \fbox{\fbox{1}\fbox{2}\fbox{0}} \fbox{\fbox{2}\fbox{0}\fbox{1}}}
    \fbox{\fbox{\fbox{1}\fbox{2}\fbox{0}} \fbox{\fbox{2}\fbox{0}\fbox{1}} \fbox{\fbox{0}\fbox{1}\fbox{2}}}
    \fbox{\fbox{\fbox{2}\fbox{0}\fbox{1}} \fbox{\fbox{0}\fbox{1}\fbox{2}} \fbox{\fbox{1}\fbox{2}\fbox{0}}}

So for our example we'll look at :math:`x=19`. In order to visualize each
definition more effectively, notice the following patterns:

* Each group of three boxes follows the pattern :math:`rotate(a, 0), rotate(a, 1), rotate(a, 2)`
* Each box at :math:`x`, when substituting, would produce the larger box at :math:`x`

This becomes very useful when you index in the same base as the sequence. So in
this case, :math:`x=19=201_3`. Because it is three digits long, we need three
rounds of substitution, and within that subsequence:

* 2 refers to the last outer group (which is rotated by two from the first group)
* 0 refers to the first subgroup
* 1 refers to the middle number in this subgroup (or index zero if rotated by one)

Because `Extended Definition Four`_ keeps existing portions static, we only
need to consider the substitutions in particular locations. If you do this at
the digits in base n, you get:

.. math::

    \tuple{0,1,2}_2 = 2 \rightarrow \tuple{2,0,1}

    \tuple{2,0,1}_0 = 2 \rightarrow \tuple{2,0,1}

    \tuple{2,0,1}_1 = 0

Since the substitution rules are defined by rotations, this can be further
rewritten as:

.. math::

    rotate(\tuple{0,1,2}, 2) = \tuple{2,0,1}

    rotate(\tuple{2,0,1}, 0) = \tuple{2,0,1}

    rotate(\tuple{2,0,1}, 1) = \tuple{0,1,2}

    \tuple{0,1,2}_0 = 0

In other words:

.. math::

    rotate(\range{n}, \sum^{\ceil*{\log_n(x)}}_{k=0} \floor*{\dfrac{x}{n^k}})_0

In addition, we have tested their equivalence in sequences in the range
:math:`x \in [0, 2^{24}], n \in [2, 2^{15}]`.

++++++++++++++++++++++++++++++++++++++++++++++
Showing Equivalence of Extensions Five And Six
++++++++++++++++++++++++++++++++++++++++++++++

:math:`increment()` and :math:`rotate()` are equivalent on the classes of tuple
used in this paper. We can show this using the relations layed out in
`Showing Equivalence of Extensions One And Three`_.

.. math::

    \begin{aligned}
    increment(\tuple{x,x+1,x+2,\hdots,x+n-1},y)_0 &= \tuple{x+y,x+y+1,x+y+2,\hdots, x+y+n-1}_0 \pmod{n} &\\
    &= rotate(\tuple{x,x+1,x+2,\hdots,x+n-1}, y)_0 \pmod{n}
    \end{aligned}

===================================
Prouhet-Terry-Escott Generalization
===================================

`Extended Definition Six`_ can be described as a family of normalized Latin
Squares, as described in [BORZ]_. The family in question looks like:

.. math::

    L_n = \left[\begin{array}{ ccccc }
        0 & 1 & 2 & ... & n-1 \\
        1 & 2 & \ddots & n-1 & 0 \\
        2 & \ddots & n-1 & 0 & 1 \\
        \vdots & n-1 & 0 & 1 & 2 \\
        n-1 & 0 & 1 & 2 & \ddots \\
    \end{array}\right]

Their substitution rules work as follows:

.. math::

    \begin{aligned}
      L_n(w) &= \pi_0(w) \| \pi_1(w) \| \pi_2(w) \| ... &\\
    \pi_x(w) &= \pi_x(a_0) \| \pi_x(a_1) \| \pi_x(a_2) \| ... &\\
    \pi_x(a) &= \tuple{x + a} \pmod{n} &\\
      T_n(x) &= L^{\ceil*{log_n(x)}}_n(\tuple{0})_x
    \end{aligned}

For comparison, `Extended Definition Six`_ could be described as:

.. math::

    \begin{aligned}
    L_T(\tuple{0}) &= \range{n} \pmod{n} &\\
    L_T^x(\tuple{0}) &= \concat{n-1}{r=0} increment(L_T^x(\tuple{0}), r) \pmod{n} &\\
    T_n(x) &= L_T^{\ceil*{\log_n(x)}}(\tuple{0})_x
    \end{aligned}

If you rewrite :math:`increment()` to look like the :math:`\pi` functions above,
then it begins to look very similar.

.. math::

    \begin{aligned}
    increment(seq, x) &= \comprehension{ele + x}{ele}{seq} \pmod{n} &\\
                      &= \concat{length(seq)-1}{r=0} \pi_x(ele) \pmod{n} &\\
                      &= \pi_x(a_0) \| \pi_x(a_1) \| \pi_x(a_2) \| ... &\\
             \pi_x(a) &= \tuple{x + a} \pmod{n}
    \end{aligned}

Thus we can rewrite `Extended Definition Six`_ to be:

.. math::

    \begin{aligned}
      L_T(w) &= \pi_0(w) \| \pi_1(w) \| pi_2(w) \| ... &\\
    \pi_x(w) &= \pi_x(a_0) \| \pi_x(a_1) \| \pi_x(a_2) \| ... &\\
    \pi_x(a) &= \tuple{x + a} \pmod{n} &\\
      T_n(x) &= L^{\ceil*{\log_n(x)}}_T(\tuple{0})_x
    \end{aligned}

Since these are equal, we know that we can encode the Thue-Morse sequence as a
normalized Latin Square, as required by [BORZ]_. To demonstrate that :math:`L_z`
is PTE-generalized, we just need to demonstrate that it complies with Theorem
3.7.1, as laid out in [BORZ]_. It goes as follows:

1. :math:`\tuple{0}` is :math:`(-1)`-regular
2. :math:`\range{n}` is :math:`0`-regular
3. :math:`L_n(\tuple{0}) = \range{n}`
4. Therefore :math:`L_n` is PTE-generalized
5. Therefore :math:`L_n^x(\tuple{0})` is :math:`(x-1)`-regular

Conjecture for future analysis:

.. math::

    ∃ L_n | L_n = G_{n,v} \texttt{ for all polynomial } v()

========================
On The Topic Of Fairness
========================

<aside> Here's the graph I've been playing with this in: https://www.desmos.com/calculator/5bimdmslt6

q() is equality. a() is the alternating turn order. t() is the Thue-Morse
sequence. v() is the value function that you can mess with. And there is a line
drawn at the expected limit value. In addition, there is a slider n for the
number of players, and a slider k for an easy way to deal with probability.
</aside>

Fairness is a fairly abstract concept to talk about, but one of the main
features of the Thue-Morse sequence is that it captures an intuitive notion of
fairness. The working definition we're trying to capture here is "A turn order
is fair if you don't reasonably care which player number you get", similar to
John Rawls' "veil of ignorance" definition. For the purpose of this paper, we
will define a minimally fair turn order as any turn order which satisfies:

.. math::

    ∀p \in \range{n}, \qquad \lim_{i \to \infty}
    \dfrac{\sum\limits_{x=0}^{i} v(x) \cdot ¬(p - t_n(x))}{\sum\limits_{x=0}^{i} v(x)} = \dfrac{1}{n} \pm ε

Where:

- :math:`p` is a player's number
- :math:`n` is the number of players
- :math:`i` is the number of turns
- :math:`v()` is a function which returns the average value of that move
- :math:`t_n()` is the turn order function
- :math:`¬` is the Boolean negation symbol. So for :math:`x \neq 0: ¬0 = 1, ¬x = 0`
- :math:`ε` is a small, player-specific number such that :math:`\tfrac{1}{n} \gg ε`

Since the above describes a *minimally* fair turn order, it is a good idea to
expand on what can make a turn order more fair. We have arrived at three
additional heuristics:

1. The faster it converges to :math:`\frac{1}{n} \pm ε`, the more fair the turn order.
2. The more biased it is towards one side before converging, the less fair the turn order.
3. The smaller the :math:`ε`, the more fair the turn order. Note that some :math:`v()`'s will *require* large :math:`ε`'s.

The turn orders we will examine are :math:`T_n` (the generalized Thue-Morse
sequence), :math:`A_n` (the usual, alternating turn order defined as
:math:`x \texttt{ mod } n`), and :math:`G_{n,v}` (the "greedy" turn order, where
each player makes a move when their *a priori* chance of victory is lower than
all other players).

+++++++++++++++++++++++++++++++
Fairness in the Constant Domain
+++++++++++++++++++++++++++++++

The Constant Domain can be defined as any function like:

.. math::

    v(x) = C

This is a special case of the Polynomial Domain, but we'll work through the
logic independently.

:math:`T_n` for this domain is fair by construction. To see why, observe the
substitution definition. For each turn in the turn order, you're replacing it
with an additional turn for each player. This means that after each :math:`n`
turns the accumulated value of each player is equal. And since you're comparing
to total accumulated value, this difference approaches 0 over time.

:math:`A_n` is also fair on this domain, but significantly less so. It both
takes longer to converge and, before converging, favors some players more often
than others.

:math:`G_{n,v}` is somewhat chaotic, depending on how you choose which player to
switch to if they have equal value. If you choose based on player number then it
is certainly less fair than :math:`T_n`, but it is reasonable to think that
there is a choosing scheme that would fix that.

+++++++++++++++++++++++++++++++++
Fairness in the Polynomial Domain
+++++++++++++++++++++++++++++++++

For our purposes, the Exponential Domain is any turn value function which looks
like:

.. math::

    \begin{aligned}
    v(x) &= ax^y + bx^{y-1} + ... + C \; where \; y \in ℕ &\\
    v(x) &= a(i-x)^y + b(i-x)^{y-1} + ... + C \; where \; i, y \in ℕ
    \end{aligned}

Note that :math:`i` is still the total number of turns, and that any conclusion
about one value function will be applicable to the second, since they are
effectively just using a reversed turn order.

At limit, or when all but one coefficient is 0, this domain is easy because it
utilizes the Prouhet-Terry-Escott problem. Because :math:`T_n` can solve that
problem, it also is fair for the above. The second definition has the same
logic, but with a reversed turn order.

For functions which have multiple non-zero coefficients, it seems like it would
also work prior to the limit, since each term still works individually under the
Prouhet-Terry-Escott problem, and addition is commutative.

I suspect, but am not certain, that it would work for a non-integer :math:`y`.

:math:`A_n` is in some instances equally fair. The spread at any given point
tends to be larger, but its "center of mass" moves around less. Until we have a
more formal way to quantify these properties, I'll just have to say that it's
"slightly less fair" and move on.

:math:`G_{n,v} = T_n`. This is further explained in the next section.

++++++++++++++++++++++++++++++++++
Fairness in the Exponential Domain
++++++++++++++++++++++++++++++++++

The Exponential domain can be defined as any function like:

.. math::

    \begin{aligned}
    v(x) &= a \cdot b^{cx} + C &\\
    v(x) &= a \cdot b^{c(i-x)} + C
    \end{aligned}

This domain can be further divided into those with :math:`b<1`, :math:`b=1` and
:math:`b>1`.

-----
b < 1
-----

In Greedy Galois Games [JCAD]_, Cooper and Dutle look at :math:`G_{n,f}` for
:math:`n=2`. They analyzed the family of functions generated by
:math:`v(x) = \tfrac{1}{k} (\tfrac{k-1}{k})^x`, the value function that one
would use for a dual where each shooter is equally good. They concluded that
:math:`\lim\limits_{k \to \infty} G_{n,v} = T_n`.

Replicating some of their work, :math:`k=4` would have a bias of ~1% towards
player 0 (compared to :math:`G_{n,v}`). At :math:`k=8` the bias is ~0.11%. This
trend generally holds true as :math:`k` gets larger.

Extending that value function to a :math:`n=3` (ignoring that a 3-way duel has a
very different optimal strategy), you find that the necessary values of
:math:`k` to converge gets larger. :math:`k=3` would have a bias of ~1%, but you
need :math:`k=10` to get a bias of ~0.11%.

-----
b = 1
-----

This case is essentially :math:`k=\infty`, so :math:`G_{n,v} = T_n`. In
addition, this is identitcal to the Constant Domain.

-----
b > 1
-----

Looking at it in a graph, the function family
:math:`v(x) = \tfrac{1}{k} (\tfrac{k+y}{k})^x`, it appears that it produces a
semi-random scatter around :math:`\frac{1}{n}`, where the spread is roughly
:math:`\frac{y}{2k}`. However, there's another scaling factor in there somewhere
that I can't seem to isolate.

However, we can reasonably assume that
:math:`\lim\limits_{k \to \infty} G_{n,v} = T_n`.

++++++++++++++++++++++++++++++
Fairness in the Modular Domain
++++++++++++++++++++++++++++++

The following is based on graphical evidence.

The Modular Domain is any turn value function which looks like:

.. math::

    v(x) = x \texttt{ mod } y \; where \; y \in ℤ^+

This domain appears to be divided in terms of fairness. When :math:`y` is even,
:math:`T_n` is fair. When :math:`y` is odd, it is usually not. I am not entirely
sure what makes an odd modulus fair or not fair, but 13 and 19 appear close,
whereas 15 and 17 appear to be biased towards player 1.

Interestingly, an alternating turn order has almost the opposite results. For
odd numbers it appears fair, and for even it appears to be biased towards
player 1.

===================
Notational Appendix
===================

++++++++++++++
Tuple Indexing
++++++++++++++

In general, a subscript will indicate that you are indexing a tuple. In this
paper, all indexing starts from 0 unless otherwise noted. For example:

.. math::

    \begin{aligned}
    a &= \tuple{0, 1, 2, 3, 4, 5, 6} &\\
    a_3 &= 3
    \end{aligned}

+++++++++++++++++++
Thue-Morse Notation
+++++++++++++++++++

The Thue-Morse sequence does not lend itself to neat notation. The cleanest way
we've found to write it requires you to change how it is indexed as a sequence.
It is easiest to instead think of it as a function which returns the correct
index of the Thue-Morse sequence.

So for example, :math:`T_n` refers to the entire Thue-Morse sequence with an
alphabet of size :math:`n`. Since there is already a subscript there, you can't
cleanly use the notation layed out in the previous section. Instead, we treat it
as a function. So :math:`T_n(17)` refers to the number at index :math:`17` in
the Thue-Morse sequence with alphabet size :math:`n`.

++++++++++++++
Number Domains
++++++++++++++

In the process of writing this paper, it was brought to my attention that
:math:`ℕ` may be ambiguous. So, to be clear, for the purposes of this paper
:math:`ℕ = \{0, 1, 2, 3, ...\}` and :math:`ℤ^+ = \{1, 2, 3, ...\}`.

++++++++++++++++++++
Tuple Comprehensions
++++++++++++++++++++

This shorthand from functional programming languages is used several times
thoughout the paper. This is equivalent to transforming each element in
:math:`sequence` by :math:`f()`, but you can also think of it as:

.. math::

    \comprehension{f(x)}{x}{sequence} = \concat{length(sequence)}{r=1} \tuple{f(sequence_r)}

++++++++++++++++
Function Symbols
++++++++++++++++

This paper makes heavy use of functional symbols. :math:`\floor*{.}` and
:math:`\ceil*{.}` represent the floor and ceiling functions. :math:`¬` is the
Boolean negation symbol. So for :math:`x \neq 0: ¬0 = 1, ¬x = 0`

==========
References
==========

.. [EUWE] Mengentheoretische Betrachtungen Über das Schachspiel,
    Proceedings Koninklijke Nederlandse Akademie van Wetenschappen, Amsterdam, Vol. 32 (5): 633-642, 1929.

.. [RMAN] Recursive Binary Sequences of Differences
    http://www.complex-systems.com/pdf/13-4-3.pdf

.. [JCAD] Greedy Galois Games
    http://people.math.sc.edu/cooper/ThueMorseDueling.pdf

.. [BORZ] The Prouhet-Tarry-Escott Problem and Generalized Thue-Morse Sequences
    https://arxiv.org/pdf/1304.6756.pdf

.. [A007413] A squarefree (or Thue-Morse) ternary sequence: closed under 1->123, 2->13, 3->2. Start with 1.
    https://oeis.org/A007413
